const https     = require('https');
const http      = require('http');
const fs        = require('fs');
const xml       = require('xml2js');
const mysql     = require('mysql');
const config    = require('./config');

let con;

// Establishing connection

new Promise((resolve, reject) => {
    con = mysql.createConnection({
        host: config.host,
        user: config.user,
        password: config.password
    });
    resolve();

}).then(
    function onFullfilled() {
        return new Promise((resolve, reject) => {
            con.connect(err => {
                if (err) reject(err);
                console.log('STATUS: CONNECTED');

                // Checking if everything necessary exists. If not -> create missing items and wait for every step to complete

                Promise.all([
                    new Promise((res, rej) => {
                        con.query('CREATE DATABASE IF NOT EXISTS lenta CHARACTER SET utf8 COLLATE utf8_general_ci;', err => {
                            if (err) rej( new Error('DATABASE CREATION >> ' + err) );
                            res();
                        });
                    }),
                    new Promise((res, rej) => {
                        con.query('USE lenta;', err => {
                            if (err) rej( new Error('DATABASE SELECTION >> ' + err) );
                            console.log('CURRENT DATABASE: lenta');
                            res();
                        }); 
                    }),
                    new Promise((res, rej) => {
                        const createTableQuery = 'CREATE TABLE IF NOT EXISTS news (news_id INT AUTO_INCREMENT PRIMARY KEY NOT NULL, title VARCHAR(150), description TEXT, pubDate DATETIME, enclosure VARCHAR(150), guid VARCHAR(100), category VARCHAR(50));';
        
                        con.query(createTableQuery, err => {
                            if (err) rej( new Error('TABLE CREATION >> ' + err) );
                            res();
                        });
                    })

                ]).then(resolve, reject);
            });
        });
    },
    function onRejected(err) {
        console.log('UNABLE TO ESTABLISH CONNECTION TO THE DATABASE\n<' + err + '>');
    }

).then(
    function onFullfilled() {

        // Sending GET request to https://lenta.ru/rss/news with a 10 mins interval

        getNews();
        setInterval(getNews, 1000 * 60 * config.interval);

        const server = http.createServer((req, res) => {

            // Request pattern identification

            const defaultPattern  = /\/addnews\/\d+$/;
            const byDatePattern   = /\/addnews\/\d{4}-\d{2}-\d{2}to\d{4}-\d{2}-\d{2}\/\d+$/; 

            if (req.url === '/index.html') {

                fs.readFile('../index.html', (err, html) => {
                    if (err) {
                        res.writeHead(404);
                        res.write('Not Found!');
                    } 
                    else {
                        res.writeHead(200, {'Content-type': 'text/html'});
                        res.write(html);
                    }
                    res.end();
                });
            }

            else if (req.url === '/css/style.css') {

                fs.readFile('../css/style.css', (err, css) => {
                    if (err) {
                        res.writeHead(404);
                        res.write('Not Found!');
                        res.end();
                    } 
                    else {
                        res.writeHead(200, {'Content-type': 'text/css'});
                        res.end(css);
                    }
                });
            }

            else if (/.js$/.test(req.url)) {
                
                fs.readFile(req.url.substring(4), (err, js) => {
                    if (err) {
                        res.writeHead(404);
                        res.write('Not Found!');
                        res.end();
                    } 
                    else {
                        res.writeHead(200, {'Content-type': 'text/javascript'});
                        res.end(js);
                    }
                });
            }

            else if (defaultPattern.test(req.url)) {

                const offset = +req.url.substring(9);

                new Promise((res, rej) => {
                    con.query('SELECT * FROM news ORDER BY pubDate DESC LIMIT 9 OFFSET ?;', [offset], (error, result) => {
                        if (error) rej( new Error('UNABLE TO RETRIEVE DATA FROM THE DATABASE ' + error) );

                        const articles = JSON.stringify(result);
                        res(articles);
                    });

                }).then(
                    function onFullfilled(value) {
                        res.writeHead(200, {'Content-type': 'application/json'});
                        res.write(value);
                        res.end();
                    },
                    function onRejected(err) {
                        console.log(err);
                    }
                );
            }

            else if (byDatePattern.test(req.url)) {

                const offset = +req.url.substring(32);
                const dates = req.url.match(/\d{4}-\d{2}-\d{2}to\d{4}-\d{2}-\d{2}/)[0].split('to');

                new Promise((res, rej) => {
                    con.query('SELECT * FROM news WHERE pubDate > ? AND pubDate < ? ORDER BY pubDate DESC LIMIT 9 OFFSET ?;', [dates[0], dates[1], offset], (error, result) => {
                        if (error) rej( new Error('UNABLE TO RETRIEVE DATA FROM THE DATABASE ' + error) );

                        const articles = JSON.stringify(result);
                        res(articles);
                    });

                }).then(
                    function onFullfilled(value) {
                        res.writeHead(200, {'Content-type': 'application/json'});
                        res.write(value);
                        res.end();
                    },
                    function onRejected(err) {
                        console.log(err);
                    }
                );
            }   

        }).listen(config.port);
    },
    function onRejected(err) {
        console.log(err);
    }
);

// GET request function declaration

function getNews() {
    console.log(new Date + ': Searching for new articles...');

    https.get('https://lenta.ru/rss/news', res => {
        let lastRec = null;
        let data = '';

        res.on('data', chunk => {
            data += chunk;
        });

        // Getting the date of the last record

        res.on('end', () => {
            new Promise((resolve, reject) => {
                con.query('SELECT pubDate FROM news ORDER BY pubDate DESC LIMIT 1;', (err, result) => {
                    if (err) reject(err);
                    if (result[0] !== undefined) {
                        lastRec = result[0].pubDate;
                    }
                    resolve();               
                });

            }).then(  // Appending new records to the 'news'
                function onFullfilled() {
                    const parser = new xml.Parser();

                    parser.parseString(data, (err, result) => {
                        if (err) throw err;

                        let counter = 0;
                        const newsArr = Array.from(
                            result['rss']['channel'][0]['item']
                        );

                        for (let i = 0; i < newsArr.length; i++) {
                            let curRec = new Date(newsArr[i].pubDate);

                            // break if the current record has the same or more recent publication date than the last record
                            if (+curRec <= +lastRec) break;

                            con.query(`INSERT INTO news VALUES (
                                    NULL, ?, ?, ?, ?, ?, ?);`, [
                                        newsArr[i].title, 
                                        newsArr[i].description,
                                        curRec,
                                        newsArr[i].enclosure[0]['$'].url, 
                                        newsArr[i].guid, 
                                        newsArr[i].category
                                    ],
                            err => { if (err) throw err; });
                            counter++;
                        }

                        console.log( 'New items added: ' + counter );
                    });
                }
                
            ).catch(err => {
                console.log('ERROR DURING REQUEST HANDLING\n<' + err + '>');
            });           
        });
    });
}