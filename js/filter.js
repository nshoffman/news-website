const filter = (function() {

    //Default values for date filter -> Current month and year

    const today = new Date();
    const now = {
        year: today.getFullYear(),
        month: today.getMonth()
    };

    // Show/hide additional settings (Date filter)

    const settingsVisisble = {
        'false': [false, 'Показать дополнительные настройки'], 
        'true': [true, 'Скрыть дополнительные настройки']
    };

    //DOM Elements definition

    const calDiv = document.getElementById('dates');
    const fromDiv = document.querySelector('#from.calendar');
    const toDiv = document.querySelector('#to.calendar');
    const showSettings = document.getElementById('show-filtering');

    //Calendars for both FROM and TO values input

    const calendarFrom = new SimpleCalendar(now.year, now.month);
    const calendarTo   = new SimpleCalendar(now.year, now.month);

    //DOM <option>'s for years available in date filter

    function getYearList() {
        let list = '';
        for (let i = calendarFrom.yearFrom; i <= calendarFrom.yearTo; i++) {
            list += `<option>${i}</option>`;
        }
        fromDiv.querySelector('.yearValue').innerHTML = list;
        toDiv.querySelector('.yearValue').innerHTML = list;
    }

    //Date conversion

    function getSelectedDate(div, cal) {
        const date = div.querySelector('td.selected');
        if (!date) return;
        return new Date(cal.currentYear, cal.currentMonth, +date.innerText);
    }

    //Year and month values update

    function updateCal(div, cal) {
        const monthValue = div.querySelector('.month .month-placeholder');
        const yearValue = div.querySelector('.yearValue');
        monthValue.innerText = cal.getMonthName();
        yearValue.value = +yearValue.value === cal.currentYear ? yearValue.value : cal.currentYear;
        fillDates(div, cal);
    }

    //Filling in the calendar body

    function fillDates(div, cal) {
        const tab = div.querySelector('.daysTab');
        let content = '';

        for (let i = 1; i <= cal.getDays(); ) {
            content += '<tr>'
            for (let j = 0; j < 7; j++, i++) {
                if (i > cal.getDays()) break;
                content += `<td>${i}</td>`;
            }
            content += '</tr>';
        }
        tab.innerHTML = content;
    }

    //Show/hide additional settings (Date filter) button event listener

    showSettings.addEventListener('click', function(e) {
        calDiv.hidden = settingsVisisble[!calDiv.hidden][0];
        this.firstElementChild.innerText = settingsVisisble[!calDiv.hidden][1];        
    });

    //Set of calendar event listeners

    {
        //Calendars reference via ID

        const calReference = {
            'from': [fromDiv, calendarFrom],
            'to': [toDiv, calendarTo]
        };

        //Switching to prev/next month

        calDiv.addEventListener('click', function(e) {

            const t = e.target;
            if (!t.classList.contains('month-btn')) return;     
    
            let root = e.target;    
            while (!root.classList.contains('calendar')) {
                root = root.parentNode;
            }            
    
            switch (t.classList[1]) {
                case 'prev':
                    calReference[root.id][1].getPrevMonth();                    
                    break;
                case 'next':
                    calReference[root.id][1].getNextMonth();
                    break;
            }
            updateCal(...calReference[root.id]);
        });

        //Date cell select

        calDiv.addEventListener('click', function(e) {

            const t = e.target;
            let root = e.target;
            if (t.tagName != 'TD') return;

            while (true) {
                if (root.tagName === 'TABLE') break;
                root = root.parentNode;
                if (root == this) return;
            }

            const selected = root.querySelector('td.selected');
            if (selected) selected.classList.remove('selected');
            t.classList.add('selected');
        });

        //Year switch listener

        calDiv.addEventListener('change', function(e) {

            const t = e.target;
            if (t.tagName != 'SELECT') return;

            let root = e.target;            
            while (!root.classList.contains('calendar')) {
                root = root.parentNode;
            }
            const targetCalendar = calReference[root.id][1];

            targetCalendar.currentYear = +t.value;
            updateCal(...calReference[root.id]);
        });
    }

    //Start

    getYearList();
    updateCal(fromDiv, calendarFrom);
    updateCal(toDiv, calendarTo);

    //Interface available for action.js

    return {
        getFromDate: function() {
            return getSelectedDate(fromDiv, calendarFrom);
        },
        getToDate: function() {
            return getSelectedDate(toDiv, calendarTo);
        }
    };

})();