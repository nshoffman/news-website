(function() {

    const datesDiv    = document.getElementById('dates');         // Date filter container
    const main        = document.getElementById('main-wrapper');  // News container
    const load        = document.getElementById('more');          // Content loading button
    const applyBtn    = document.getElementById('filter-apply');  // Filter apply button
    const cancelBtn   = document.getElementById('filter-cancel'); // Filter dismiss button
    let request     = 'addnews/';                                 // Request string
    let offset      = 0;                                          // Current 'news' table offset

    // News-item component

    function NewsItem(title, description, category, date, link, prev) {

        const div = document.createElement('div');
        div.className = 'news-item';

        const content = `
        <div class="news-item--preview">
            <img src="${prev}" alt="News item preview image...">
            <p class="news-item--category">${category}</p>
        </div>
        <h3 class="news-item--title">${title}</h3>
        <p class="news-item--description">${description}</p>
        <a class="news-item--link" href="${link}">Читать полностью</a>
        <p class="news-item--date">Опубликовано ${date}</p>`;

        div.innerHTML = content;
        return div;
    }

    // Two-digit format conversion

    function zerofy(n) {
        return n > 9 ? n : '0' + n;
    }

    // DD-MM-YYYY format conversion

    function stringifyDate(date, req) {
        const year    = date.getFullYear();
        const month   = zerofy(date.getMonth() + 1);
        const day     = zerofy(date.getDate());

        if (req === undefined) {
            const hours   = zerofy(date.getHours());
            const mins    = zerofy(date.getMinutes());

            return `${day}.${month}.${year} ${hours}:${mins}`;
        }

        return `${year}-${month}-${day}`;        
    }

    // DOM rendering out of JSON data

    function addNews(json) {
        // Row definition for filling in with news-items

        const rows = main.querySelectorAll('div.news-row.clearfix');
        let lastChild;
        let row;

        // If rows do exist in current DOM -> check the last one first

        if (rows.length) {                                       
            lastChild = rows[rows.length - 1];
        }

        // If the last row in the DOM is already full -> create a new one
        
        if (lastChild && lastChild.children.length < 3) {
            row = lastChild;
        } else {
            row = document.createElement('div');
            row.classList.add('news-row', 'clearfix');
        }

        for (let i = 0; i < json.length; i++) {
            
            row.appendChild(
                NewsItem(
                    json[i].title,
                    json[i].description,
                    json[i].category,
                    stringifyDate( new Date(json[i].pubDate) ),
                    json[i].guid,
                    json[i].enclosure
                )
            );

            if (row.children.length == 3) {
                main.appendChild(row);
                row = document.createElement('div');
                row.classList.add('news-row', 'clearfix');
            }
        }
    }

    // GET request

    function sendRequest() {
        const xml = new XMLHttpRequest();
        xml.open('GET', request + offset);

        xml.onload = function() {
            const result = JSON.parse(this.responseText);
            addNews(result);

            // If no more news are left -> hide load button

            switch (result.length < 9) {                    
                case true:
                    if (!load.hidden) {
                        load.hidden = true;
                        offset += 9;
                    }
                    break;
                case false:
                    if (load.hidden) {
                        load.hidden = false;
                    }
                    offset += 9;
                    break;
            }
        };
        xml.send();
    }

    // Filter cancellation

    function dropFilter() {
        datesDiv.classList.remove('error');
        [].forEach.call(datesDiv.querySelectorAll('td.selected'), cell => {
            cell.classList.remove('selected');
        });

        request = 'addnews/';
        offset = 0;
        main.innerHTML = '';
        sendRequest();
        return;
    }

    // Applying date filter to the request settings

    function changeRequest() {

        const dateA = filter.getFromDate();
        const dateB = filter.getToDate();

        if (dateA >= dateB
            || 
            (!dateA || !dateB)) 
        {
            datesDiv.classList.add('error');
            return;
        }

        datesDiv.classList.remove('error');
        request = `addnews/${stringifyDate(dateA, true)}to${stringifyDate(dateB, true)}/`;
        offset = 0;
        main.innerHTML = '';
        sendRequest();
    }

    applyBtn.onclick = changeRequest;
    cancelBtn.onclick = dropFilter;
    load.onclick = sendRequest;

    sendRequest();

}());
