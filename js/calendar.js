class SimpleCalendar {

    constructor(year, month) {
        this.currentMonth = month;
        this.currentYear = year;
    }

    getMonthList() {
        return [
                'Январь',
                'Февраль',
                'Март',
                'Апрель',
                'Май',
                'Июнь',
                'Июль',
                'Август',
                'Сентябрь',
                'Октябрь',
                'Ноябрь',
                'Декабрь'
        ];
    }    
    getPrevMonth() {
        if (this.currentMonth === 0 && this.currentYear === this.yearFrom) {
            return;
        } else if (this.currentMonth === 0) {
            this.currentMonth = 11;
            this.getPrevYear(); 
        } else {
            this.currentMonth -= 1;
        }
    }
    getNextMonth() {
        if (this.currentMonth === 11 && this.currentYear === this.yearTo) {
            return;
        } else if (this.currentMonth === 11) {
            this.currentMonth = 0;
            this.getNextYear();
        } else {
            this.currentMonth += 1;
        }
    }
    getMonthName() {
        return this.getMonthList()[this.currentMonth];
    }    
    getNextYear() {
        this.currentYear += this.currentYear < this.yearTo ? 1 : 0;
    }
    getPrevYear() {
        this.currentYear -= this.currentYear > this.yearFrom ? 1 : 0;
    }

    getDays() {
        return new Date(this.currentYear, this.currentMonth + 1, 0).getDate();
    }
};

SimpleCalendar.prototype.yearFrom = 2015;
SimpleCalendar.prototype.yearTo = new Date().getFullYear();