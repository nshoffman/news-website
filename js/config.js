const config = {
    host: 'localhost',
    user: 'root',
    password: 'root',
    port: 8080,
    interval: 10
}

module.exports = config;