# NewsFoundry

NewsFoundry is a website collecting and parsing articles from [Lenta.ru API](https://lenta.ru/info/posts/export/).

## Installation

Simply use Git to clone the repository into local directory on your PC

## Launch

Use terminal to execute **app.js** to run the server and set up the environment

```bash
node app.js
```

If necessary, use **config.js** to change the connection settings

## Access

NewsFoundry website is available via following link:
>localhost:{port}/index.html
